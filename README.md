# Servidor de Archivos Dockerizado

Creación de dos contenedores para el montaje de un servidor de archivos.
Contenedor de Nextcloud y de PostgresQL.

En el script principal se muestra la instalación de los contenedores, además
se observa también la el nombre de los mismos, la base de datos que se usará
en el contenedor de postgres y el puerto por el cual funcionará le nextcloud.

Adicionalmente hay dos scripts lo bastante sencillos para la realización de
las copias de respaldo y también para el incio automático de los contenedores.



