#!/bin/bash

# Creación de objetos de docker.

docker volume create backUp
docker volume create data
docker volume create nextcloud

# Contenedor de PostgresQL.

docker run \
	   --name postgresql \
	   --mount source=backUp,destination=/home \
	   --mount source=data,destination=/var/lib/postgresql/data \
	   -e POSTGRES_PASSWORD=postgres \
	   -e POSTGRES_DB=cloud \
	   --detach \
	   postgres

# Contenedor de nextcloud

docker run \
	   --name nextcloud \
	   --link postgresql:cloud \
	   -p 9090:80 \
	   --mount source=nextcloud,destination=/var/www/html \
	   --detach \
	   nextcloud
