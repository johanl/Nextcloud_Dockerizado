#!/bin/bash

rdiff-backup /var/lib/docker/volumes/data /media/backUp/data
rdiff-backup /var/lib/docker/volumes/nextcloud /media/backUp/nextcloud
docker exec -it postgresql pg_dump -U postgres cloud > /media/backUp/backUp/arquiler-cloud-backUp.dump

